import React, { useState } from "react";
import { SendOutlined } from "@ant-design/icons";

export default function ChatInput({ handleSendMsg }) {
  const [msg, setMsg] = useState("");

  const sendChat = (event) => {
    event.preventDefault();
    if (msg.length > 0) {
      handleSendMsg(msg);
      setMsg("");
    }
  };

  return (
    <>
      <form className="input-container" onSubmit={(event) => sendChat(event)}>
        <input
          className="chat-input"
          type="text"
          placeholder="type your message here"
          onChange={(e) => setMsg(e.target.value)}
          value={msg}
        />
        <button className="send-button" type="submit">
          <SendOutlined />
        </button>
      </form>
    </>
  );
}
