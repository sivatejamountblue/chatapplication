import React, { Component } from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Register from './pages/Register';
import Login from './pages/Login';
import Chat from './pages/Chat';
import 'antd/dist/antd.min.css';

class App extends Component {
  state = {  } 
  render() { 
    return (
      // <div>Chat App</div>
      <BrowserRouter>
        <Routes>
          <Route path="/register" element={<Register />} />
          <Route path="/login" element={<Login />} />
          <Route path="/" element={<Chat />} />
        </Routes>

      </BrowserRouter>
    );
  }
}
 
export default App;
